﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml.Serialization;
using scores.Components.Scores;

namespace scores.Models
{
    [Serializable()]
    public class ScoresViewModel
    {
        public ScoresViewModel()
        {
            Scores = ESPNScores.getScores();
        }

        public Scores Scores { get; set; }
    }

    public class Scores
    {
        public string Delay { get; set; }
        public List<Game> Games { get; set; }
        public string Sport { get; set; }
    }

    public class Game
    {
        public string GameId { get; set; }
        public Team AwayTeam { get; set; }
        public Team HomeTeam { get; set; }
        public string StatusId { get; set; }
        public string Status { get; set; }
        public string StatusLine { get; set; }
        public string Url { get; set; }
        public string UrlType { get; set; }
    }

    public class Team
    {
        public string Name { get; set; }
        public string Score { get; set; }
    }
}

