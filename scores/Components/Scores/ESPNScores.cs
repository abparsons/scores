﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Xml.Serialization;
using System.IO;
using scores.Components.Scores.Classes;

namespace scores.Components.Scores
{
    public class ESPNScores
    {
        public static Models.Scores getScores()
        {
            SCORES _games = null;
            string path = "http://wu.apple.com/ncf/bottomline/xml/scores";

            WebResponse objResponse;
            WebRequest objRequest = HttpWebRequest.Create(path);
            objResponse = objRequest.GetResponse();

            XmlSerializer serializer = new XmlSerializer(typeof(SCORES));

            StreamReader reader = new StreamReader(objResponse.GetResponseStream());
            _games = (SCORES)serializer.Deserialize(reader);
            reader.Close();

            Models.Scores scores = new Models.Scores();
            scores.Delay = _games.DELAY;
            scores.Sport = _games.sport;

            if (_games.GAME != null)
            {
                List<SCORESGAME> finishedGames = (from g in _games.GAME
                                                  where g.STATUS.Contains("FINAL")
                                                  select g).ToList<SCORESGAME>();
                List<SCORESGAME> games = _games.GAME.Except(finishedGames).ToList<SCORESGAME>();
                games.AddRange(finishedGames);

                scores.Games = (from g in games
                                select new Models.Game
                                {
                                    GameId = g.GAMEID,
                                    StatusId = g.STATUSID,
                                    Status = g.STATUS,
                                    StatusLine = g.STATUSLINE,
                                    Url = g.URL,
                                    UrlType = g.URLTYPE,
                                    AwayTeam = new Models.Team
                                    {
                                        Name = g.AWAY[0].TEAM,
                                        Score = g.AWAY[0].SCORE,
                                    },
                                    HomeTeam = new Models.Team
                                    {
                                        Name = g.HOME[0].TEAM,
                                        Score = g.HOME[0].SCORE
                                    }
                                }).ToList<Models.Game>();
            }
            else scores.Games = new List<Models.Game>();

            return scores;
        }
    }
}