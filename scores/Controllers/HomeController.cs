﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using scores.Models;
using System.Xml.Serialization;
using System.IO;
using System.Net;

namespace scores.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Scores";

            var viewModel = new ScoresViewModel();

            return View(viewModel);
        }
    }
}
